This application is created to display a flower chosen from the list of options in the list box

Once the flower is selected, there are two display options available. The first is a black white photo of the flower selected, and the second is the colored version of that photo.

After the picture display is chosen, there are two additional options, which will display the Latin name of the plant chosen as well as the color of the flower when checked.