﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AGuaranOutOfClass2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string flower;

            if (listBox1.SelectedIndex != -1)
            {
                // Get the selected item
                flower = listBox1.SelectedItem.ToString();

                // Create variables for the images which are located in the resources folder
                Image BWRose = Properties.Resources.B_W_Rose;
                Image CRose = Properties.Resources.Color_Rose;
                Image BWTulip = Properties.Resources.B_W_Tulip;
                Image CTulip = Properties.Resources.Color_Tulip;
                Image BWDahlia = Properties.Resources.B_W_Dahlia;
                Image CDahlia = Properties.Resources.Color_Dahlia;
                Image BWBP = Properties.Resources.B_W_BirdOfParadise;
                Image CBP = Properties.Resources.Color_BirdOfParadise;
                Image BWChrys = Properties.Resources.B_W_Chrysanthemum;
                Image CChrys = Properties.Resources.Color_Chrysanthemum;

                // Determine the flower that will be displayed.
                switch (flower)
                {
                    case "Rose":
                        //Make sure the user checks one of the radio buttons, to display an either black and white or color picture
                        if (radioButton1.Checked == true)
                        {
                            pictureBox1.Image = BWRose;
                        }
                        else if (radioButton2.Checked == true)
                        {
                            pictureBox1.Image = CRose;
                        }
                        else
                        {
                            MessageBox.Show("Please choose the type of picture");
                        }
                        //Displays the latin name of the plant chosen if the box is checked
                        if (checkBox1.Checked == true)
                        {
                            label1.Text = ("Rosa hesperrhodos");
                        }
                        else
                        {
                            label1.Text = "".ToString();
                        }
                        //Displays the color of the flower in the picture if the box is checked
                        if (checkBox2.Checked == true)
                        {
                            label2.Text = ("Red");
                        }
                        else
                        {
                            label2.Text = "".ToString();
                        }
                        break;
                    case "Tulip":
                        if (radioButton1.Checked == true)
                        {
                            pictureBox1.Image = BWTulip;
                        }
                        else if (radioButton2.Checked == true)
                        {
                            pictureBox1.Image = CTulip;
                        }
                        else
                        {
                            MessageBox.Show("Please choose the type of picture");
                        }
                        if (checkBox1.Checked == true)
                        {
                            label1.Text = ("Tulipa gesneriana");
                        }
                        else
                        {
                            label1.Text = "".ToString();
                        }
                        if (checkBox2.Checked == true)
                        {
                            label2.Text = ("Yellow");
                        }
                        else
                        {
                            label2.Text = "".ToString();
                        }
                        break;
                    case "Dahlia":
                        if (radioButton1.Checked == true)
                        {
                            pictureBox1.Image = BWDahlia;
                        }
                        else if (radioButton2.Checked == true)
                        {
                            pictureBox1.Image = CDahlia;
                        }
                        else
                        {
                            MessageBox.Show("Please choose the type of picture");
                        }
                        if (checkBox1.Checked == true)
                        {
                            label1.Text = ("Dahlia pinnata");
                        }
                        else
                        {
                            label1.Text = "".ToString();
                        }
                        if (checkBox2.Checked == true)
                        {
                            label2.Text = ("Pink");
                        }
                        else
                        {
                            label2.Text = "".ToString();
                        }
                        break;
                    case "Bird of Paradise":
                        if (radioButton1.Checked == true)
                        {
                            pictureBox1.Image = BWBP;
                        }
                        else if (radioButton2.Checked == true)
                        {
                            pictureBox1.Image = CBP;
                        }
                        else
                        {
                            MessageBox.Show("Please choose the type of picture");
                        }
                        if (checkBox1.Checked == true)
                        {
                            label1.Text = ("Strelitzia reginae");
                        }
                        else
                        {
                            label1.Text = "".ToString();
                        }
                        if (checkBox2.Checked == true)
                        {
                            label2.Text = ("Orange");
                        }
                        else
                        {
                            label2.Text = "".ToString();
                        }
                        break;
                    case "Chrysanthemum":
                        if (radioButton1.Checked == true)
                        {
                            pictureBox1.Image = BWChrys;
                        }
                        else if (radioButton2.Checked == true)
                        {
                            pictureBox1.Image = CChrys;
                        }
                        else
                        {
                            MessageBox.Show("Please choose the type of picture");
                        }
                        if (checkBox1.Checked == true)
                        {
                            label1.Text = ("Chrysanthemum indicum");
                        }
                        else
                        {
                            label1.Text = "".ToString();
                        }
                        if (checkBox2.Checked == true)
                        {
                            label2.Text = ("White");
                        }
                        else
                        {
                            label2.Text = "".ToString();
                        }
                        break;
                }
            }
            else
            {
                //Ensures that an option is selected in the list box when the display button is clicked
                MessageBox.Show("Please select a flower");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {   //Clears all the text displayed as well as the picture
            label1.Text = "".ToString();
            label2.Text = "".ToString();
            pictureBox1.Image = null;
        }
    }
}
